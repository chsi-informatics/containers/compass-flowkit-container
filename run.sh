#!/usr/bin/env bash

# Set environment variables
RSTUDIO_SERVER_IMAGE="compass-flowkit-ubuntu-focal.sif"
PORT=8788

#
# Disabling authentication when running locally.
# In Rstudio Server 1.4, something changed related to requiring a token
# when logging into the server.
#
export SINGULARITYENV_RSTUDIO_SESSION_TIMEOUT=0  # not sure if this is still necessary

#
# Start RStudio Server
#
# Create temp directory structure that Rstudio needs (will bind these later)
export RSTUDIO_TMP_DIR
RSTUDIO_TMP_DIR="$(mktemp -d)"
mkdir -p "${RSTUDIO_TMP_DIR}"/run
mkdir -p "${RSTUDIO_TMP_DIR}"/tmp
mkdir -p "${RSTUDIO_TMP_DIR}"/rstudio-server

# Create Config File
cat > "${RSTUDIO_TMP_DIR}"/rserver.conf <<END
www-port=${PORT}
server-user=$USER
END

# Launch the RStudio Server
echo "Starting up rserver..."
echo "-----------------------------------"
echo "  Port: ${PORT}"
echo "  User: ${USER}"
echo "  Temp directory: ${RSTUDIO_TMP_DIR}"
echo "https://localhost:${PORT}"
echo "-----------------------------------"

singularity exec \
  -B ${RSTUDIO_TMP_DIR}/tmp:/tmp \
  -B ${RSTUDIO_TMP_DIR}/run:/run \
  -B ${RSTUDIO_TMP_DIR}/rstudio-server:/var/lib/rstudio-server \
  -B ${RSTUDIO_TMP_DIR}/rserver.conf:/etc/rstudio/rserver.conf \
  ${RSTUDIO_SERVER_IMAGE} rserver --config-file /etc/rstudio/rserver.conf
