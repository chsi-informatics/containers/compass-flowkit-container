# compass-container

## Overview

This repository contains the Singularity definition for building a container containing the following:
* COMPASS R package
* FlowKit Python package

See the compass-singularity.def file for a complete listing of included libraries.

Services include Rstudio server & Jupyter.

The definition file builds directly from the official Ubuntu 20.04 (Focal) Docker image.

## Build

`sudo singularity build compass-flowkit-ubuntu-focal.sif compass-flowkit-singularity.def`

## Pull Image File

`singularity pull oras://gitlab-registry.oit.duke.edu/chsi-informatics/containers/compass-flowkit-container/compass-flowkit-container:latest`

## Run

Running the container launches an Rstudio session from the container's `/opt/notebooks` directory on port `8888`. Any directory bindings must be explicitly set when issuing the `singularity run` command.

### Run

#### Rstudio Server

Run the Rstudio server by executing the included `run.sh` shell script. 

#### Jupyter
```
singularity exec compass-flowkit-ubuntu-focal.sif jupyter notebook
```
